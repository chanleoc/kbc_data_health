# Terms and Conditions

The Data Health Application is a custom science application built and offered by Leo as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to filter or cleanse unwanted data from their desired dataset. 
Feel free to use the code and please report any unencountered bugs or errors.

## Contact

Leo Chan   
Vancouver, Canada (PST time)   
Email: leo@keboola.com   
Private: cleojanten@hotmail.com   