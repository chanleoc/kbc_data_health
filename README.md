# README #
This repository is a collection of functions which can aid users to cleanse or filther unwanted data form their report for further report analysis.

### Full Documentation ###

For all the descriptions and configurations  
Please visit the link [here](https://docs.google.com/document/d/e/2PACX-1vQn8q70iRchorL7lvsaJpp5qrXbArug5FcIjmrECH1aZ6gyttJs3T0idqmK58UPkLPnmFZEFz4qK54D/pub).     


### Contact Info ###

Leo Chan  
Vancouver, Canada (PST time)  
Email: leo@keboola.com  
Private: cleojanten@hotmail.com  
