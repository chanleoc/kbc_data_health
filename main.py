import os
import sys
import dateparser
import datetime  # noqa
import re
from keboola import docker
import pandas as pd
import json
import csv  # noqa
import logging
import logging_gelf.handlers
import logging_gelf.formatters


"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"

"""d
Python 3 environment 
"""

# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:

    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'), port=int(os.getenv('KBC_LOGGER_PORT')))
    logging_gelf_handler.setFormatter(
        logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)

    # remove default logging to stdout
    logger.removeHandler(logger.handlers[0])

# get proper list of tables
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

# Logging version
logging.info('Data Health Application Version: {}'.format('0.1.1'))

# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
KEYS = cfg.get_parameters()["primarykey"]
RULES = cfg.get_parameters()["rules"]
logging.info("Number of rules: {0}".format(len(RULES)))

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))
"""
out_tables = cfg.get_expected_output_tables()
logging.info("OUT tables mapped: "+str(out_tables))
"""

# Setup failed_outputs column headers
rejected_header = []
# for i in KEYS:
#    rejected_header.append(i)
rejected_header.append("FAILED_COLUMN")
rejected_header.append("CONDITION")


def split_list(target):
    """
    Splitting a string into a list with "," as delimiter
    """
    breakup = target.split(",")
    result = []
    for i in breakup:
        result.append(i)
    return result


def validate_config_rules(rules_config, df):
    """
    validate whether or not the configuration file is good
    rules does not qualify will not be implemented
    1. confirm if there are any rules
    2. confirm if the inserted column header in configuration is one of the file's header
        convert the input string into a list with "," as delimiter
        - if not, that rule will be ignored
    3. confirm if there are any duplicates in the "list" to compare -- (LIST)
        - if found, removing from the list
    4. remove any spaces if rule is a numerical comparison -- (NUMERIC)
    5. validate if the logical operators make sense -- (NUMERIC)
    6. validate input regex -- (REGEX)
    7. validate length parameter -- (LENGTH)
    8. validate if "value_type" is supported -- (VALUE_TYPE)
    """
    # rule 1: if there are no rules, then exit (no point...)
    if len(rules_config) == 0:
        logging.info("INFO: There are no rules to implement.")
        logging.info("Exit")
        sys.exit(0)

    temp_rule = []
    reject_rule = []
    for i in rules_config:
        add_rule_bool = False
        # rule 2
        if i['column_name'] in list(df.columns):
            ########################
            # rule 3 -- LIST
            if i['type'] == "list":
                value_list = replace(i['value'], " , ", ",")
                value_list = replace(value_list, " ,", ",")
                value_list = replace(value_list, ", ", ",")
                i['value'] = split_list(value_list)
                temp_list = []
                for k in i['value']:
                    if k in temp_list:
                        logging.warning(
                            "RULES WARNING: Value ({0}) in list is duplicated.".format(k))
                    if k not in temp_list:
                        temp_list.append(k)
                # do not run rule, if array is empty
                if len(temp_list) == 0:
                    logging.warning(
                        "RULES WARNING: There are values to compare in list({0}).".format(i['column_name']))
                else:
                    i['value'] = temp_list
                    add_rule_bool = True
            ########################
            # rule 4 & rule 5 -- NUMERIC
            if i['type'] == "numeric":
                # remove any spaces in between strings
                temp_value = ""
                temp_value = replace(i['value'], " ", "")
                i['value'] = temp_value
                # validate rule
                add_rule_bool = validate_oper(i['value'])
                if not add_rule_bool:
                    logging.error("RULES ERROR: Invalid comparison argument for {0}: {1} ".format(
                        i['column_name'], i['value']))
            ########################
            # rule 6 -- REGEX
            if i['type'] == "regex":
                add_rule_bool = check_regex(i['value'])
                if not add_rule_bool:
                    logging.error("RULES ERROR: Invalid regex input for {0}: {1}".format(
                        i['column_name'], i['value']))
            ########################
            # rule 7 -- LENGTH
            if i['type'] == "length":
                add_rule_bool = i['value'].isnumeric()
                if not add_rule_bool:
                    logging.error("RULES ERROR: Invalid length parameter for {0}: {1}".format(
                        i['column_name'], i['value']))
            ########################
            # rule 8 -- VALUE_TYPE
            if i['type'] == "value_type":
                value_list = ["null", "string", "numeric", "date", "json"]
                i['value'] = i['value'].lower()
                if i['value'] in value_list:
                    add_rule_bool = True
                else:
                    logging.error("RULES ERROR: Invalid value_type parameter {0}: {1}".format(
                        i['column_name'], i['value']))
            ########################
            # if neither rule failed, then skip rule
            if add_rule_bool:
                temp_rule.append(i)
            if not add_rule_bool:
                reject_rule.append(i)

        else:
            logging.error("RULES ERROR: column_name ({0}) is not found in the source file.".format(
                i['column_name']))
            reject_rule.append(i)

    # if there isnt any rules to run after verification, then exit
    if len(temp_rule) == 0:
        logging.info("INFO: There are no rules to implement.")
        logging.info("Exit")
        sys.exit(0)

    return temp_rule, reject_rule


def get_tables(in_tables):  # , out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """
    # input file
    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    """
    #good output file
    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    #bad output file
    table = out_tables[1]
    out_name_1 = table["full_path"]
    out_Destination_1 = table["source"]
    logging.info("Output table: " + str(out_name_1))
    logging.info("Output table destination: " + str(out_Destination_1))
    """

    return in_name  # , out_name, out_name_1


def lookup(target, a_list):
    """ lookup if target matches with list provided """
    # change variable type if needed
    for i in a_list:
        if str(target) in str(i):
            return True
    return False


def length_check(target, count):
    """ Number of Digits/length check"""
    # count = Number
    if len(str(target)) == int(count):
        return True
    return False


def replace(target, a, b):
    """ replace a with b """
    result = target.replace(a, b)
    return result


def compare_oper(target, value):
    """ 
    verify if the target is "expression) than value 
    output: bool
    cases:
    1--> ">10"
    2--> "10<x<11"
    """
    # starting index for the first digit
    digit_index = re.search("\d", value)
    # case 1: <"XX"
    # if first index is not digit
    if digit_index.start() != 0:
        try:
            return eval(str(target)+value)
        except Exception:
            return False
    # case 2: 10<x<11
    # if first index is not digit
    if digit_index.start() == 0:
        try:
            return eval(value.split("x")[0]+target+value.split("x")[1])
        except Exception:
            return False

    return False


def validate_oper(value):
    """ confirm if the expression is valid """
    # starting index for the first digit
    digit_index = re.search("\d", value)
    # case 1: <"xx"
    if digit_index.start() != 0:
        return True
    # case 2: 10<x<20
    elif digit_index.start() == 0:
        # creating a midvalue between the values provided to evaluate whether or not the expression makes sense
        first_part = value.split("x")[0]
        second_part = value.split("x")[1]
        first_value = first_part[:(re.search("\d", first_part)).end()]
        second_value = second_part[re.search("\d", second_part).start():]
        midvalue = (int(first_value)+int(second_value))/2
        return eval(first_part+str(midvalue)+second_part)
    else:
        return False


def check_regex(reg):
    """ confirm if the regex is valid """
    try:
        re.compile(reg)
        return True
    except Exception:
        return False


def run_regex(target, reg):
    """ verify if the input target qualifies the input regex """
    try:
        reg = re.compile(reg)
        (re.match(reg, target)).group()
        return True
    except Exception:
        return False


def compare_type(target, v_type):
    """ 
    verify if the input target is a type of v_type 
    Supported: ["null","string","numeric","date","json"]
    "string" == not null & not numeric & not date & not json
    """
    truth = True
    string_truth = True
    # null
    if v_type == "null" or v_type == "string":
        truth = not df_isempty(target)
        string_truth = string_truth and not truth
    # numeric
    if v_type == "numeric" or v_type == "string":
        truth = target.isnumeric()
        string_truth = string_truth and not truth
    # date
    if v_type == "date":
        try:
            dateparser.parse(target)
            print(dateparser.parse(target))
            if not dateparser.parse(target):
                truth = False
            else:
                truth = True
        except Exception:
            truth = False
    # json
    if v_type == "json" or v_type == "string":
        try:
            parsing = json.loads(target)
            if not parsing or target.isnumeric():
                truth = False
                string_truth = string_truth and not truth
            else:
                truth = True
                string_truth = string_truth and not truth
        except Exception:
            truth = False
            string_truth = string_truth and not truth

    # returns
    if v_type == "string":
        return string_truth
    else:
        return truth


def isempty(target):
    if target:
        return False
    else:
        return True


def df_isempty(target):
    """ find out the target in df is empty """
    return pd.isnull(target)


def healthcheck(itr, new_rules):
    """
    determine if the row passes healthcheck, will iterate till all rules are covered
    if not return a df with reasons why it was rejected
    output: (bool whether it passes HC , failed_df)
    """

    number_of_rules = len(new_rules)
    fail_df = pd.DataFrame(columns=rejected_header)
    validation = True
    n = 0
    while n < number_of_rules:
        truth, temp_df = run_rule(n, itr)
        validation = validation and truth
        fail_df = fail_df.append(temp_df, ignore_index=True)

        n += 1

    return validation, fail_df


def run_rule(n, itr):
    """
    Sub function of Healthcheck function for recursive runs
    running ONE rule in this function
    input:
        n: number of rules index
        itr: data file row index
    output:
        truth: whether the specified rule pass or fail
        fail_df: return a row of df if rule failed
    """
    
    columns = new_rules[n]["column_name"]
    truth = True
    fail_df = pd.DataFrame(columns=list(rejected_header))
    error_msg = ""

    ########################
    # rule type = length
    if new_rules[n]['type'] == "length":
        truth = length_check(data_df[columns][itr], int(new_rules[n]["value"]))
        if not truth:
            logging.info("Length Validation Rule: {0}({1}) - not {2} index long".format(
                str(data_df[columns][itr]), columns, new_rules[n]["value"]))
            error_msg = str(new_rules[n]["value"])+" digits only"
    ########################
    # rule type = list
    if new_rules[n]['type'] == "list":
        truth = lookup(data_df[columns][itr], new_rules[n]["value"])
        if not truth:
            logging.info(
                "List Matching Rule: {0}({1}) - not in the list".format(str(data_df[columns][itr]), columns))
            error_msg = "Does not match provided list"  # message for rejected rows
    ########################
    # rule type = numeric
    if new_rules[n]['type'] == "numeric":
        truth = compare_oper(data_df[columns][itr], new_rules[n]["value"])
        if not truth:
            logging.info("Value Comparision Rule: {0}({1}) - not {2}".format(
                str(data_df[columns][itr]), columns, new_rules[n]["value"]))
            # message for rejected rows
            error_msg = "Not {0}".format(new_rules[n]["value"])
    ########################
    # rule type = regex
    if new_rules[n]['type'] == "regex":
        truth = run_regex(data_df[columns][itr], new_rules[n]["value"])
        if not truth:
            logging.info("Regex Rule: {0}({1}) does not qualify.".format(
                str(data_df[columns][itr]), columns))
            error_msg = "Does not qualify regex".format(
                new_rules[n]["value"])  # message for rejected rows
    ########################
    # rule type = value_type
    if new_rules[n]['type'] == "value_type":
        truth = compare_type(data_df[columns][itr], new_rules[n]["value"])
        if not truth:
            if new_rules[n]["value"] == "null":
                logging.info("value_type Rule: {0}({1}) - is {2}".format(
                    str(data_df[columns][itr]), columns, new_rules[n]["value"]))
                # message for rejected rows
                error_msg = "is {0}".format(new_rules[n]["value"])
            else:
                logging.info("value_type Rule: {0}({1}) - not {2}".format(
                    str(data_df[columns][itr]), columns, new_rules[n]["value"]))
                # message for rejected rows
                error_msg = "Not {0}".format(new_rules[n]["value"])

    # create a df for the failing row
    if not truth:
        temp_list = []
        # for i in KEYS:
        for i in list(data_df.columns):
            # instead of PK, outputting all columns from the input file
            temp_list.append(data_df[i][itr])  # data_df[i][itr] = primary key
        temp_list.append(str(columns))  # FAILED_COLUMN
        temp_list.append(error_msg)  # CONDITION
        temp_df = pd.DataFrame([temp_list], columns=list(rejected_header))
        fail_df = fail_df.append(temp_df, ignore_index=True)

    return truth, fail_df


def output_file(data_output, file_output, column_headers):
    """ 
    Output the dataframe input to destination file
    * row by row
    """
    if not os.path.isfile(file_output):
        with open(file_output, 'a') as b:
            data_output.to_csv(b, index=False, columns=column_headers)
        b.close()
    else:
        with open(file_output, 'a') as b:
            data_output.to_csv(b, index=False, header=False,
                               columns=column_headers)
        b.close()

    return


if __name__ == "__main__":
    """
    Main execution script.
    """
    new_list = []

    # Input and output files' names
    """in_file, out_file, out_file_1 = get_tables(in_tables,out_tables)"""
    in_file = get_tables(in_tables)
    good_output = DEFAULT_FILE_DESTINATION + \
        (in_file.split(".csv")[0]).split("/data/in/tables/")[1]+"_passed.csv"
    bad_output = DEFAULT_FILE_DESTINATION + \
        (in_file.split(".csv")[0]).split("/data/in/tables/")[1]+"_rejected.csv"

    # Success fail gauge
    output_n = 0
    failed_n = 0

    # Data_df row gauge
    itr = 0

    # reading the input as df
    for data_df in pd.read_csv(in_file, dtype=str, chunksize=10000):
        # data = pd.read_csv(in_file, dtype=str)
        # data_df = pd.DataFrame(data)
        full_length = len(data_df) + itr  # lenght of the input file
        if itr == 0:
            rejected_header = list(data_df.columns) + rejected_header

            ###############################
            # validate if the config is good
            logging.info("List of columns in source file: {0}".format(
                list(data_df.columns)))
            column_header = list(data_df.columns)
            new_rules, rejected_rules = validate_config_rules(RULES, data_df)
            logging.info("Valid Rules Running: {0}".format(len(new_rules)))

        ###############################
        # Setup output, fail_output dataframe and parameters
        # empty dataframe for outputing the failed rows in HC
        failed_output = pd.DataFrame(columns=rejected_header)

        # iterate row by row for health check
        while itr < full_length:
            # find out whether the row is valid
            # there will be something in failed_output if result = False
            result, failed_output = healthcheck(itr, new_rules)

            if result:
                # for rows passed all the rules
                output = pd.DataFrame(data_df, index=[itr])
                output_file(output, good_output, column_header)
                output_n += 1
                # logging.info("Line {0}: Passed".format(itr+1))
            else:
                # fetch the failed_output from healthcheck results
                output_file(failed_output, bad_output, rejected_header)
                failed_n += 1
                # logging.info("Line {0}:Failed".format(itr+1))

            itr += 1

        logging.info("Processed [{}] records...".format(itr))

    # Output empty file for failed_output
    # if zero row failed

    if failed_n == 0:
        output_file(failed_output, bad_output, rejected_header)
    # Output Empty file for good_output
    elif output_n == 0:
        empty_df = pd.DataFrame(columns=data_df.columns.tolist())
        output_file(empty_df, good_output, data_df.columns.tolist())

    # Output rules stats\
    logging.info("Number of Processed Rules: {0}".format(len(new_rules)))
    logging.info("Processed Rules: {0}".format(new_rules))
    logging.info("Number of Rejected Rules: {0}".format(len(rejected_rules)))
    if len(rejected_rules) != 0:
        logging.info("Rejected Rules: {0}".format(rejected_rules))

    logging.info("Good Data: {0}".format(output_n))
    logging.info("Failed Data: {0}".format(failed_n))
    # print(in_file)

logging.info("Done.")
